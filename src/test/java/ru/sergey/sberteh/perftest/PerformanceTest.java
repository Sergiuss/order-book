package ru.sergey.sberteh.perftest;

import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import ru.sergey.sberteh.ApplicationContext;
import ru.sergey.sberteh.book.OrderBookManager;
import ru.sergey.sberteh.book.OrderListener;
import ru.sergey.sberteh.model.Order;
import ru.sergey.sberteh.parser.OrderXmlHandler;
import ru.sergey.sberteh.parser.OrdersXmlParser;
import ru.sergey.sberteh.util.ThreadsMediator;

public class PerformanceTest {
    private static final String PATH = "/home/sergey/Downloads/orders.xml";

    private final ApplicationContext applicationContext = new ApplicationContext();
    private final OrdersXmlParser xmlParser;
    private final OrderBookManager bookManager;

    public PerformanceTest() {
        applicationContext.init();

        xmlParser = applicationContext.getBean(OrdersXmlParser.class);
        bookManager = applicationContext.getBean(OrderBookManager.class);
    }

    @Before
    public void setUp() {
        bookManager.clear();
    }

    @Test
    public void readAndProcess() {
        System.out.println("Direct read and process orders test");

        final long startTime = System.currentTimeMillis();
        xmlParser.parseOrdersXml(PATH);
        final long totalProcessingTime = System.currentTimeMillis() - startTime;

        System.out.println("Processing time: " + totalProcessingTime + "ms");
        System.out.println("Processed order count: " + bookManager.getProcessedOrderCount());
        System.out.println();
    }

    @Test
    public void readToMemoryAndProcess() throws ParserConfigurationException, SAXException {
        System.out.println("Read to memory and process orders test");

        final OrdersHolder ordersHolder = new OrdersHolder();
        final OrderXmlHandler orderXmlHandler = new OrderXmlHandler(ordersHolder);
        final OrdersXmlParser ordersXmlParser = new OrdersXmlParser(orderXmlHandler);

        ordersXmlParser.parseOrdersXml(PATH);

        final long startTime = System.currentTimeMillis();

        for (int i = 0; i < ordersHolder.orders.size(); i++) {
            bookManager.onOrder(ordersHolder.orders.get(i));
        }

        final long totalProcessingTime = System.currentTimeMillis() - startTime;

        System.out.println("Processing time: " + totalProcessingTime + "ms");
        System.out.println("Processed order count: " + bookManager.getProcessedOrderCount());
        System.out.println();
    }

    @Test
    public void readIntoQueueAndProcess() throws ParserConfigurationException, SAXException {
        System.out.println("Read into queue and process orders test");

        final ThreadsMediator mediator = new ThreadsMediator(bookManager);
        final OrderXmlHandler orderXmlHandler = new OrderXmlHandler(mediator);
        final OrdersXmlParser ordersXmlParser = new OrdersXmlParser(orderXmlHandler);

        mediator.start();

        final long startTime = System.currentTimeMillis();
        ordersXmlParser.parseOrdersXml(PATH);

        // await all orders
        while (bookManager.getProcessedOrderCount() != 2776106) {
            Thread.yield();
        }

        final long totalProcessingTime = System.currentTimeMillis() - startTime;

        mediator.stop();

        System.out.println("Processing time: " + totalProcessingTime + "ms");
        System.out.println("Processed order count: " + bookManager.getProcessedOrderCount());
        System.out.println();
    }

    private class OrdersHolder implements OrderListener {
        private final List<Order> orders = new ArrayList<>();

        @Override
        public void onOrder(final Order order) {
            orders.add(order);
        }
    }
}
