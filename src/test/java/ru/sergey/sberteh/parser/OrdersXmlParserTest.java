package ru.sergey.sberteh.parser;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.xml.sax.SAXException;

import ru.sergey.sberteh.book.OrderListener;

public class OrdersXmlParserTest {
    private final OrderListener orderListener = Mockito.mock(OrderListener.class);
    private final OrderXmlHandler orderXmlHandler = new OrderXmlHandler(orderListener);
    private OrdersXmlParser ordersXmlParser;

    @Before
    public void setUp() throws ParserConfigurationException, SAXException {
        ordersXmlParser = new OrdersXmlParser(orderXmlHandler);

        Mockito.reset(orderListener);
    }

    @Test
    public void parserTest() {
        final String path = this.getClass().getClassLoader().getResource("test-orders.xml").getPath();

        ordersXmlParser.parseOrdersXml(path);

        Mockito.verify(orderListener).onOrder(Mockito.any());
    }
}
