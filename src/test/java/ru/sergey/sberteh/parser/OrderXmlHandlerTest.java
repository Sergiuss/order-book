package ru.sergey.sberteh.parser;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import ru.sergey.sberteh.book.OrderListener;
import ru.sergey.sberteh.model.AddOrder;
import ru.sergey.sberteh.model.DeleteOrder;
import ru.sergey.sberteh.model.Order;

public class OrderXmlHandlerTest {
    private final OrderListener orderListener = Mockito.mock(OrderListener.class);

    private final XMLReader xmlReader;

    public OrderXmlHandlerTest() throws ParserConfigurationException, SAXException {
        final SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setNamespaceAware(true);
        final SAXParser saxParser = spf.newSAXParser();
        xmlReader = saxParser.getXMLReader();
        xmlReader.setContentHandler(new OrderXmlHandler(orderListener));
    }

    @Before
    public void setUp() {
        Mockito.reset(orderListener);
    }

    @Test
    public void emptyOrdersDocumentTest() throws IOException, SAXException {
        xmlReader.parse(createInputSource("<Orders></Orders>"));
        Mockito.verify(orderListener, never()).onOrder(any());
    }

    @Test
    public void addOrderTest() throws IOException, SAXException {
        xmlReader.parse(createInputSource("<Orders><AddOrder book='book-1' orderId='12' volume='500' price='12.45' operation='SELL' /></Orders>"));
        Mockito.verify(orderListener).onOrder(expectedOrder(new AddOrder()));
    }

    @Test
    public void deleteOrderTest() throws IOException, SAXException {
        xmlReader.parse(createInputSource("<Orders><DeleteOrder book='book-1' orderId='12' /></Orders>"));
        Mockito.verify(orderListener).onOrder(expectedOrder(new DeleteOrder()));
    }

    private InputSource createInputSource(final String xml) {
        return new InputSource(new ByteArrayInputStream(xml.getBytes()));
    }

    private Order expectedOrder(final Order order) {
        order.setBook("book-1");
        order.setOrderId(12);

        return order;
    }
}
