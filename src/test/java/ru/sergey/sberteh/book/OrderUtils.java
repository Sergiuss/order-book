package ru.sergey.sberteh.book;

import ru.sergey.sberteh.model.AddOrder;
import ru.sergey.sberteh.model.DeleteOrder;
import ru.sergey.sberteh.model.Operation;

public final class OrderUtils {
    private OrderUtils() {}

    public static AddOrder createAddOrder(final Integer id, final double price, final int volume, final Operation op, final String orderBookId) {
        final AddOrder addOrder = createAddOrder(id, price, volume, op);

        addOrder.setBook(orderBookId);

        return addOrder;
    }

    public static AddOrder createAddOrder(final Integer id, final double price, final int volume, final Operation op) {
        final AddOrder addOrder = new AddOrder();

        addOrder.setOrderId(id);
        addOrder.setPrice(price);
        addOrder.setVolume(volume);
        addOrder.setOperation(op);

        return addOrder;
    }

    public static DeleteOrder createDeleteOrder(final Integer id, final String orderBookId) {
        final DeleteOrder deleteOrder = createDeleteOrder(id);

        deleteOrder.setBook(orderBookId);

        return deleteOrder;
    }

    public static DeleteOrder createDeleteOrder(final Integer id) {
        final DeleteOrder deleteOrder = new DeleteOrder();

        deleteOrder.setOrderId(id);

        return deleteOrder;
    }
}
