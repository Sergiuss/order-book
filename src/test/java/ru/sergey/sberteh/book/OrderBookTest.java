package ru.sergey.sberteh.book;

import static org.junit.Assert.assertEquals;
import static ru.sergey.sberteh.book.OrderUtils.createDeleteOrder;

import java.util.Collection;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;

import ru.sergey.sberteh.model.AddOrder;
import ru.sergey.sberteh.model.DeleteOrder;
import ru.sergey.sberteh.model.Operation;


public class OrderBookTest {
    private static final Logger LOGGER = Logger.getLogger(OrderBookTest.class.getName());

    private final OrderBook orderBook = new OrderBook("book-1");
    private Integer sequence = 0;

    @Before
    public void setUp() {
        orderBook.clear();
        sequence = 0;
    }

    @Test
    public void addBidAskNonMatchedTest() {
        final AddOrder addOrderBuy = createAddOrder(100.1, 10, Operation.BUY);
        final AddOrder addOrderSell = createAddOrder(100.2, 11, Operation.SELL);

        orderBook.addOrder(addOrderBuy);
        orderBook.addOrder(addOrderSell);

        final Collection<? extends Order> bidOrders = orderBook.getOrdersBySide(Side.BID);
        assertEquals(1, bidOrders.size());
        assertOrders(addOrderBuy, bidOrders.iterator().next());

        final Collection<? extends Order> askOrders = orderBook.getOrdersBySide(Side.ASK);
        assertEquals(1, askOrders.size());
        assertOrders(addOrderSell, askOrders.iterator().next());
    }

    @Test
    public void addBidAskMatchedFullFilledTest() {
        final AddOrder addOrderBuy = createAddOrder(100.1, 10, Operation.BUY);
        final AddOrder addOrderSell = createAddOrder(100.0, 10, Operation.SELL);

        orderBook.addOrder(addOrderBuy);
        orderBook.addOrder(addOrderSell);

        final Collection<? extends Order> bidOrders = orderBook.getOrdersBySide(Side.BID);
        assertEquals(0, bidOrders.size());

        final Collection<? extends Order> askOrders = orderBook.getOrdersBySide(Side.ASK);
        assertEquals(0, askOrders.size());
    }

    @Test
    public void addBidAskMatchedPartialFilledTest() {
        final AddOrder addOrderBuy = createAddOrder(100.1, 10, Operation.BUY);
        final AddOrder addOrderSell = createAddOrder(100.0, 5, Operation.SELL);

        orderBook.addOrder(addOrderBuy);
        orderBook.addOrder(addOrderSell);

        final Collection<? extends Order> bidOrders = orderBook.getOrdersBySide(Side.BID);
        assertEquals(1, bidOrders.size());
        addOrderBuy.setVolume(addOrderBuy.getVolume() - addOrderSell.getVolume());
        assertOrders(addOrderBuy, bidOrders.iterator().next());

        final Collection<? extends Order> askOrders = orderBook.getOrdersBySide(Side.ASK);
        assertEquals(0, askOrders.size());
    }

    @Test
    public void addBidAskMatchedPartialFilled2Test() {
        final AddOrder addOrderBuy = createAddOrder(100.1, 5, Operation.BUY);
        final AddOrder addOrderSell = createAddOrder(100.0, 10, Operation.SELL);

        orderBook.addOrder(addOrderBuy);
        orderBook.addOrder(addOrderSell);

        final Collection<? extends Order> bidOrders = orderBook.getOrdersBySide(Side.BID);
        assertEquals(0, bidOrders.size());

        final Collection<? extends Order> askOrders = orderBook.getOrdersBySide(Side.ASK);
        assertEquals(1, askOrders.size());
        addOrderSell.setVolume(addOrderSell.getVolume() - addOrderBuy.getVolume());
        assertOrders(addOrderSell, askOrders.iterator().next());
    }

    @Test
    public void addBidAskMatchedPartialFilled3Test() {
        final AddOrder addOrderBuy = createAddOrder(100.1, 10, Operation.BUY);
        final AddOrder addOrderBuy2 = createAddOrder(100.1, 15, Operation.BUY);
        final AddOrder addOrderSell = createAddOrder(100.0, 20, Operation.SELL);

        orderBook.addOrder(addOrderBuy);
        orderBook.addOrder(addOrderBuy2);
        orderBook.addOrder(addOrderSell);

        final Collection<? extends Order> bidOrders = orderBook.getOrdersBySide(Side.BID);
        assertEquals(1, bidOrders.size());

        addOrderBuy2.setVolume(5);
        assertOrders(addOrderBuy2, bidOrders.iterator().next());
    }

    @Test
    public void addAndDeleteTest() {
        final AddOrder addOrderBuy = createAddOrder(100.1, 5, Operation.BUY);

        orderBook.addOrder(addOrderBuy);

        final DeleteOrder deleteOrder = createDeleteOrder(addOrderBuy.getOrderId());

        orderBook.deleteOrder(deleteOrder);

        final Collection<? extends Order> bidOrders = orderBook.getOrdersBySide(Side.BID);
        assertEquals(0, bidOrders.size());

        final Collection<? extends Order> askOrders = orderBook.getOrdersBySide(Side.ASK);
        assertEquals(0, askOrders.size());
    }

    @Test
    public void addAndDeleteNonExistingTest() {
        final AddOrder addOrderBuy = createAddOrder(100.1, 5, Operation.BUY);

        orderBook.addOrder(addOrderBuy);

        final DeleteOrder deleteOrder = createDeleteOrder(1000);

        orderBook.deleteOrder(deleteOrder);

        final Collection<? extends Order> bidOrders = orderBook.getOrdersBySide(Side.BID);
        assertEquals(1, bidOrders.size());

        final Collection<? extends Order> askOrders = orderBook.getOrdersBySide(Side.ASK);
        assertEquals(0, askOrders.size());
    }

    @Test
    public void addOrderAndPrintTest() {
        orderBook.addOrder(createAddOrder(100.1, 10, Operation.BUY));
        orderBook.addOrder(createAddOrder(100.0, 4, Operation.BUY));
        orderBook.addOrder(createAddOrder(99.98, 98, Operation.BUY));
        orderBook.addOrder(createAddOrder(100.2, 11, Operation.SELL));
        orderBook.addOrder(createAddOrder(100.21, 14, Operation.SELL));
        orderBook.addOrder(createAddOrder(100.23, 14, Operation.SELL));
        orderBook.addOrder(createAddOrder(101.00, 12, Operation.SELL));

        LOGGER.info(orderBook.toString());

        // fill order book
        orderBook.addOrder(createAddOrder(100.2, 11, Operation.BUY));
        orderBook.addOrder(createAddOrder(100.0, 5, Operation.SELL));

        LOGGER.info(orderBook.toString());
    }

    private void assertOrders(final AddOrder addOrder, final Order order) {
        assertEquals(addOrder.getOrderId(), order.getId());
        assertEquals(addOrder.getVolume(), order.getVolume());
        assertEquals(addOrder.getPrice(), order.getPrice(), 0);
        assertEquals(Side.operationToSide(addOrder.getOperation()), order.getSide());
    }

    private AddOrder createAddOrder(final double price, final int volume, final Operation op) {
        return OrderUtils.createAddOrder(++sequence, price, volume, op);
    }
}
