package ru.sergey.sberteh.book;

import static org.junit.Assert.assertEquals;
import static ru.sergey.sberteh.book.OrderUtils.createAddOrder;
import static ru.sergey.sberteh.book.OrderUtils.createDeleteOrder;

import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;

import ru.sergey.sberteh.model.AddOrder;
import ru.sergey.sberteh.model.DeleteOrder;
import ru.sergey.sberteh.model.Operation;

public class OrderBookManagerTest {
    private static final Logger LOGGER = Logger.getLogger(OrderBookManagerTest.class.getName());

    private final OrderBookManager orderBookManager = new OrderBookManager();
    private Integer sequence = 0;

    @Before
    public void setUp() {
        orderBookManager.clear();
        sequence = 0;
    }

    @Test
    public void addOrderToOrderBookAndDeleteTest() {
        final AddOrder addOrder = createAddOrder(++sequence, 100.1, 10, Operation.BUY, "book-1");
        final AddOrder addOrder2 = createAddOrder(++sequence, 100.1, 10, Operation.SELL, "book-2");

        orderBookManager.onOrder(addOrder);
        orderBookManager.onOrder(addOrder2);

        OrderBook orderBook = orderBookManager.getOrderBook(addOrder.getBook());
        assertEquals(1, orderBook.getOrdersBySide(Side.BID).size());

        orderBook = orderBookManager.getOrderBook(addOrder2.getBook());
        assertEquals(1, orderBook.getOrdersBySide(Side.ASK).size());

        final DeleteOrder deleteOrder = createDeleteOrder(addOrder.getOrderId(), addOrder.getBook());

        orderBookManager.onOrder(deleteOrder);

        orderBook = orderBookManager.getOrderBook(addOrder.getBook());
        assertEquals(0, orderBook.getOrdersBySide(Side.BID).size());

        orderBook = orderBookManager.getOrderBook(addOrder2.getBook());
        assertEquals(1, orderBook.getOrdersBySide(Side.ASK).size());
    }

    @Test
    public void printOrderBooks() {
        orderBookManager.onOrder(createAddOrder(++sequence, 100.1, 10, Operation.BUY, "book-1"));
        orderBookManager.onOrder(createAddOrder(++sequence, 100.0, 4, Operation.BUY, "book-1"));
        orderBookManager.onOrder(createAddOrder(++sequence, 99.98, 98, Operation.BUY, "book-1"));
        orderBookManager.onOrder(createAddOrder(++sequence, 100.2, 11, Operation.SELL, "book-1"));
        orderBookManager.onOrder(createAddOrder(++sequence, 100.21, 14, Operation.SELL, "book-1"));
        orderBookManager.onOrder(createAddOrder(++sequence, 100.23, 14, Operation.SELL, "book-1"));
        orderBookManager.onOrder(createAddOrder(++sequence, 101.00, 12, Operation.SELL, "book-1"));

        orderBookManager.onOrder(createAddOrder(++sequence, 100.1, 10, Operation.BUY, "book-2"));
        orderBookManager.onOrder(createAddOrder(++sequence, 100.0, 4, Operation.BUY, "book-2"));
        orderBookManager.onOrder(createAddOrder(++sequence, 99.98, 98, Operation.BUY, "book-2"));
        orderBookManager.onOrder(createAddOrder(++sequence, 100.2, 11, Operation.SELL, "book-2"));
        orderBookManager.onOrder(createAddOrder(++sequence, 100.21, 14, Operation.SELL, "book-2"));
        orderBookManager.onOrder(createAddOrder(++sequence, 100.23, 14, Operation.SELL, "book-2"));
        orderBookManager.onOrder(createAddOrder(++sequence, 101.00, 12, Operation.SELL, "book-2"));

        LOGGER.info(orderBookManager.toString());
    }
}
