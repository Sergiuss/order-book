package ru.sergey.sberteh;

import java.util.HashMap;
import java.util.Map;

import ru.sergey.sberteh.book.OrderBookManager;
import ru.sergey.sberteh.parser.OrderXmlHandler;
import ru.sergey.sberteh.parser.OrdersXmlParser;

public class ApplicationContext {
    private final Map<Class<?>, Object> beans = new HashMap<>();

    public void init() {
        try {
            beans.put(OrderBookManager.class, new OrderBookManager());
            beans.put(OrderXmlHandler.class, new OrderXmlHandler(getBean(OrderBookManager.class)));
            beans.put(OrdersXmlParser.class, new OrdersXmlParser(getBean(OrderXmlHandler.class)));
        } catch (final Exception e) {
            throw new IllegalStateException("Context cannot be initialized", e);
        }
    }

    @SuppressWarnings("unchecked")
    public <T> T getBean(final Class<T> clazz) {
        return (T) beans.get(clazz);
    }
}
