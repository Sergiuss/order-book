package ru.sergey.sberteh.model;

public interface OrderVisitor {
    void visitAddOrder(final AddOrder order);
    void visitDeleteOrder(final DeleteOrder order);
}
