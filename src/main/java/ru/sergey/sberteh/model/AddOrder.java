package ru.sergey.sberteh.model;

public class AddOrder extends Order {
    private Operation operation;
    private double price;
    private int volume;

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(final Operation operation) {
        this.operation = operation;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(final double price) {
        this.price = price;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(final int volume) {
        this.volume = volume;
    }

    @Override
    public String toString() {
        return "AddOrder [operation=" + operation + ", price=" + price
                + ", volume=" + volume + ", book=" + getBook()
                + ", orderId=" + getOrderId() + "]";
    }

    @Override
    public void applyVisitor(final OrderVisitor visitor) {
        visitor.visitAddOrder(this);
    }
}
