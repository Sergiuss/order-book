package ru.sergey.sberteh.model;

public class DeleteOrder extends Order {
    @Override
    public String toString() {
        return "DeleteOrder [book=" + getBook() + ", orderId="
                + getOrderId() + "]";
    }

    @Override
    public void applyVisitor(final OrderVisitor visitor) {
        visitor.visitDeleteOrder(this);
    }
}
