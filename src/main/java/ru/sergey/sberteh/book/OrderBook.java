package ru.sergey.sberteh.book;

import java.util.Collection;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import ru.sergey.sberteh.model.AddOrder;
import ru.sergey.sberteh.model.DeleteOrder;

class OrderBook {
    private static final Comparator<Order> BUY_COMPARATOR = new OrderComparator(Side.ASK);
    private static final Comparator<Order> SELL_COMPARATOR = new OrderComparator(Side.BID);

    private final Map<Integer, Order> orders = new HashMap<>();
    private final EnumMap<Side, SortedSet<Order>> ordersByOperation = new EnumMap<>(Side.class);

    private final String id;

    public OrderBook(final String id) {
        ordersByOperation.put(Side.ASK, new TreeSet<>(BUY_COMPARATOR));
        ordersByOperation.put(Side.BID, new TreeSet<>(SELL_COMPARATOR));

        this.id = id;
    }

    public void addOrder(final AddOrder addOrder) {
        final Order order = convertToOrder(addOrder);

        final SortedSet<Order> ordersForMatching = ordersByOperation.get(order.getSide().otherSide());

        tryToMatchOrder(order, ordersForMatching);

        if (order.getVolume() != 0) {
            orders.put(order.getId(), order);
            ordersByOperation.get(order.getSide()).add(order);
        }
    }

    public void deleteOrder(final DeleteOrder deleteOrder) {
        final Order order = orders.remove(deleteOrder.getOrderId());

        if (order != null) {
            ordersByOperation.get(order.getSide()).remove(order);
        }
    }

    protected Collection<? extends Order> getOrdersBySide(final Side side) {
        return ordersByOperation.get(side);
    }

    public void clear() {
        orders.clear();
        ordersByOperation.get(Side.ASK).clear();
        ordersByOperation.get(Side.BID).clear();
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();

        sb.append("Order book: ").append(id).append(System.getProperty("line.separator"));
        sb.append("BID         ASK").append(System.getProperty("line.separator"));
        sb.append("Qty@Price – Qty@Price").append(System.getProperty("line.separator"));

        final Iterator<Order> bidIterator = ordersByOperation.get(Side.BID).iterator();
        final Iterator<Order> askIterator = ordersByOperation.get(Side.ASK).iterator();

        final Formatter formatter = new Formatter(sb);

        while (bidIterator.hasNext() || askIterator.hasNext()) {
            printOrder(bidIterator.hasNext() ? bidIterator.next() : null, formatter);

            sb.append("  –  ");

            printOrder(askIterator.hasNext() ? askIterator.next() : null, formatter);

            sb.append(System.getProperty("line.separator"));
        }

        sb.append(System.getProperty("line.separator"));

        return sb.toString();
    }

    private void printOrder(final Order order, final Formatter formatter) {
        if (order != null) {
            formatter .format("%3d@%3.2f", order.getVolume(), order.getPrice());
        } else {
            formatter.format("%S", "----------");
        }
    }

    private void tryToMatchOrder(final Order order, final SortedSet<Order> ordersForMatching) {
        while(!ordersForMatching.isEmpty() && order.getVolume() != 0) {
            final Order orderForMatching = ordersForMatching.first();

            final double orderPrice = order.getPrice();
            final double orderForMatchingPrice = orderForMatching.getPrice();

            if ((order.getSide() == Side.BID && Double.compare(orderPrice, orderForMatchingPrice) >= 0)
                    || (order.getSide() == Side.ASK && Double.compare(orderPrice, orderForMatchingPrice) <= 0)) {
                final int orderVol = order.getVolume();
                final int forMatchingOrderVol = orderForMatching.getVolume();

                if(orderVol >= forMatchingOrderVol){
                    order.setVolume(order.getVolume() - orderForMatching.getVolume());
                    ordersForMatching.remove(orderForMatching);
                } else {
                    orderForMatching.setVolume(orderForMatching.getVolume() - order.getVolume());
                    ordersForMatching.add(orderForMatching);
                    order.setVolume(0);
                }
            } else {
                break;
            }
        }
    }

    private Order convertToOrder(final AddOrder addOrder) {
        final Order order = new Order(addOrder.getOrderId());

        order.setPrice(addOrder.getPrice());
        order.setVolume(addOrder.getVolume());
        order.setSide(Side.operationToSide(addOrder.getOperation()));

        return order;
    }

    private static class OrderComparator implements Comparator<Order> {
        private final boolean isBid;

        public OrderComparator(final Side side) {
            if (Side.ASK == side) {
                isBid = false;
            } else {
                isBid = true;
            }
        }

        @Override
        public int compare(final Order leftOrder, final Order rightOrder) {
            if (leftOrder == rightOrder) {
                return 0;
            }

            final double leftPrice = leftOrder.getPrice();
            final double rightPrice = rightOrder.getPrice();

            if (leftPrice > rightPrice) {
                return isBid ? -1 : 1;
            } else if (leftPrice < rightPrice) {
                return isBid ? 1 : -1;
            } else {
                final long leftId = leftOrder.getId();
                final long rightId = rightOrder.getId();

                if (leftId < rightId) {
                    return -1;
                } else if (leftId > rightId) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }
    }
}
