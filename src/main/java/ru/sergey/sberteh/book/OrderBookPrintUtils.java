package ru.sergey.sberteh.book;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Formatter;
import java.util.Iterator;
import java.util.List;

public final class OrderBookPrintUtils {
    private OrderBookPrintUtils() {}


    public static String printOrderBooks(final OrderBookManager orderBookManager) {
        final StringBuilder sb = new StringBuilder();

        orderBookManager.getOrderBooks().forEach(orderBook -> {
            printOrderBook(orderBook, sb);
        });

        return sb.toString();
    }

    private static void printOrderBook(final OrderBook orderBook, final StringBuilder sb) {
        final Collection<? extends Order> bidOrders = orderBook.getOrdersBySide(Side.BID);
        final Collection<? extends Order> askOrders = orderBook.getOrdersBySide(Side.ASK);

        final List<PriceVolume> bidPriceVolumes = toPriceVolume(bidOrders);
        final List<PriceVolume> askPriceVolumes = toPriceVolume(askOrders);

        sb.append("Order book: ").append(orderBook.getId()).append(System.getProperty("line.separator"));
        sb.append("BID         ASK").append(System.getProperty("line.separator"));
        sb.append("Qty@Price – Qty@Price").append(System.getProperty("line.separator"));

        final Iterator<PriceVolume> bidIterator = bidPriceVolumes.iterator();
        final Iterator<PriceVolume> askIterator = askPriceVolumes.iterator();

        final Formatter formatter = new Formatter(sb);

        while (bidIterator.hasNext() || askIterator.hasNext()) {
            printOrder(bidIterator.hasNext() ? bidIterator.next() : null, formatter);

            sb.append("  –  ");

            printOrder(askIterator.hasNext() ? askIterator.next() : null, formatter);

            sb.append(System.getProperty("line.separator"));
        }

        sb.append(System.getProperty("line.separator"));
    }

    private static void printOrder(final PriceVolume pv, final Formatter formatter) {
        if (pv != null) {
            formatter .format("%3d@%3.2f", pv.volume, pv.price);
        } else {
            formatter.format("%S", "---------");
        }
    }

    private static List<PriceVolume> toPriceVolume(final Collection<? extends Order> orders) {
        final List<PriceVolume> priceVolumes = new ArrayList<>();

        PriceVolume pv = null;

        for (final Order order : orders) {
            if(pv != null && Double.compare(pv.price, order.getPrice()) == 0) {
                pv.volume = pv.volume + order.getVolume();
            } else {
                pv = new PriceVolume();
                pv.price = order.getPrice();
                pv.volume = order.getVolume();

                priceVolumes.add(pv);
            }
        }

        return priceVolumes;
    }

    private static class PriceVolume {
        private double price;
        private int volume;
    }
}
