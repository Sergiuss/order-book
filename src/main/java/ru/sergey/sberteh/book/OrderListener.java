package ru.sergey.sberteh.book;

import ru.sergey.sberteh.model.Order;

public interface OrderListener {
    void onOrder(final Order order);
}
