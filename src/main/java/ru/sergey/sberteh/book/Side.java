package ru.sergey.sberteh.book;

import ru.sergey.sberteh.model.Operation;

enum Side {
    BID, //
    ASK, //
    ;

    public Side otherSide() {
        if (this == BID) {
            return ASK;
        } else {
            return BID;
        }
    }

    public static Side operationToSide(final Operation operation) {
        if (Operation.BUY == operation) {
            return BID;
        } else {
            return ASK;
        }
    }
}
