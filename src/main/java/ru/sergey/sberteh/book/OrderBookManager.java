package ru.sergey.sberteh.book;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import ru.sergey.sberteh.model.AddOrder;
import ru.sergey.sberteh.model.DeleteOrder;
import ru.sergey.sberteh.model.Order;
import ru.sergey.sberteh.model.OrderVisitor;

public class OrderBookManager implements OrderListener {
    private final OrderVisitor orderVisitor = new OrderVisitorImpl();
    private final Map<String, OrderBook> orderBooks = new HashMap<>();

    private long processedOrderCount = 0;

    @Override
    public void onOrder(final Order order) {
        processedOrderCount++;
        order.applyVisitor(orderVisitor);
    }

    public long getProcessedOrderCount() {
        return processedOrderCount;
    }

    private OrderBook getOrCreateOrderBook(final String orderBookId) {
        OrderBook orderBook = orderBooks.get(orderBookId);

        if (orderBook == null) {
            orderBook = new OrderBook(orderBookId);
            orderBooks.put(orderBookId, orderBook);
        }

        return orderBook;
    }

    private class OrderVisitorImpl implements OrderVisitor {
        @Override
        public void visitAddOrder(final AddOrder order) {
            getOrCreateOrderBook(order.getBook()).addOrder(order);
        }

        @Override
        public void visitDeleteOrder(final DeleteOrder order) {
            getOrCreateOrderBook(order.getBook()).deleteOrder(order);
        }
    }

    protected OrderBook getOrderBook(final String id) {
        return orderBooks.get(id);
    }

    protected Collection<? extends OrderBook> getOrderBooks() {
        return orderBooks.values();
    }

    public void clear() {
        orderBooks.clear();
        processedOrderCount = 0;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();

        for (final OrderBook orderBook : orderBooks.values()) {
            sb.append(orderBook.toString());
        }

        return sb.toString();
    }
}
