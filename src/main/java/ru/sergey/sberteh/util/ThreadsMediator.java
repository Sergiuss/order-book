package ru.sergey.sberteh.util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Logger;

import ru.sergey.sberteh.book.OrderListener;
import ru.sergey.sberteh.model.Order;

public class ThreadsMediator implements OrderListener {
    private static final Logger LOGGER = Logger.getLogger(ThreadsMediator.class.getName());

    private final OrderListener outQueue;
    private final BlockingQueue<Order> inQueue = new ArrayBlockingQueue<Order>(10000);

    private final Runnable outQueueWorker = new OutQueueWorker();
    private Thread outQueueThread;

    private volatile boolean running = false;

    public ThreadsMediator(final OrderListener outQueue) {
        this.outQueue = outQueue;
    }

    public void start() {
        if (!running && outQueueThread == null) {
            running = true;
            outQueueThread = new Thread(outQueueWorker, "Core thread");
            outQueueThread.start();
        }
    }

    public void stop() {
        if (running && outQueueThread != null) {
            running = false;
            outQueueThread.interrupt();

            try {
                outQueueThread.join();
            } catch (final InterruptedException e) {
                LOGGER.warning("Thread is interrupted");
            }

            outQueueThread = null;
        }
    }

    @Override
    public void onOrder(final Order order) {
        try {
            inQueue.put(order);
        } catch (final InterruptedException e) {
            LOGGER.warning("Thread is interrupted");
        }
    }

    private class OutQueueWorker implements Runnable {
        private final List<Order> buffer = new ArrayList<>(1000);

        @Override
        public void run() {
            buffer.clear();

            while (running) {
                inQueue.drainTo(buffer);

                if (!buffer.isEmpty()) {
                    for (int i = 0; i < buffer.size(); i++) {
                        outQueue.onOrder(buffer.get(i));
                    }

                    buffer.clear();
                } else {
                    Thread.yield();
                }
            }
        }
    }
}
