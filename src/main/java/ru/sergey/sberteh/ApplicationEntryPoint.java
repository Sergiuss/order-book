package ru.sergey.sberteh;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import ru.sergey.sberteh.book.OrderBookManager;
import ru.sergey.sberteh.book.OrderBookPrintUtils;
import ru.sergey.sberteh.parser.OrdersXmlParser;

public class ApplicationEntryPoint {
    private static final Logger LOGGER = Logger.getLogger(ApplicationEntryPoint.class.getName());

    public static final void main(final String[] args) throws SAXException, ParserConfigurationException, IOException {
        final ApplicationContext applicationContext = new ApplicationContext();

        applicationContext.init();

        System.out.print("Enter full path to orders xml file: ");

        final String path = readLine();

        final OrdersXmlParser ordersXmlParser = applicationContext.getBean(OrdersXmlParser.class);
        final OrderBookManager bookManager = applicationContext.getBean(OrderBookManager.class);

        final long startTime = System.currentTimeMillis();
        ordersXmlParser.parseOrdersXml(path);
        final long totalProcessingTime = System.currentTimeMillis() - startTime;

        System.out.println("Processing time: " + totalProcessingTime + "ms");
        System.out.println("Processed order count: " + bookManager.getProcessedOrderCount());
        System.out.println();

        //System.out.println(bookManager.toString());
        System.out.println(OrderBookPrintUtils.printOrderBooks(bookManager));
    }

    private static String readLine() {
        final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String path = null;

        try {
            path = br.readLine();

            File file = new File(path);

            while (!file.exists()) {
                System.out.println();
                System.out.print("File does not exist. Enter existing file name: ");

                path = br.readLine();
                file = new File(path);
            }
        } catch (final IOException e) {
            LOGGER.severe("An exception occured");
        }

        return path;
    }
}
