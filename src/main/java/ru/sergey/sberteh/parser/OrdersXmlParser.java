package ru.sergey.sberteh.parser;

import java.io.IOException;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;


public class OrdersXmlParser {
    private static final Logger LOGGER = Logger.getLogger(OrdersXmlParser.class.getName());
    private final XMLReader xmlReader;

    public OrdersXmlParser(final ContentHandler contentHandler)
            throws ParserConfigurationException, SAXException {
        final SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setNamespaceAware(true);
        final SAXParser saxParser = spf.newSAXParser();
        xmlReader = saxParser.getXMLReader();
        xmlReader.setContentHandler(contentHandler);
    }

    public void parseOrdersXml(final String path) {
        try {
            xmlReader.parse(path);
        } catch (IOException | SAXException e) {
            LOGGER.severe("An error is occured while xml file processing. " + e.getMessage());
        }
    }
}
