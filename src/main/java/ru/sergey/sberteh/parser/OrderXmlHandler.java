package ru.sergey.sberteh.parser;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import ru.sergey.sberteh.book.OrderListener;
import ru.sergey.sberteh.model.AddOrder;
import ru.sergey.sberteh.model.DeleteOrder;
import ru.sergey.sberteh.model.Operation;
import ru.sergey.sberteh.model.Order;

public class OrderXmlHandler extends DefaultHandler {
    private static final Logger LOG = Logger.getLogger(OrderXmlHandler.class.getName());
    private static final String ADD_ORDER = "AddOrder";
    private static final String DELETE_ORDER = "DeleteOrder";

    private final ElementParser defaultParser = new DefaultParser();
    private final ElementParser addOrderParser = new AddOrderParser();
    private final ElementParser deleteOrderParser = new DeleteOrderParser();

    private final OrderListener orderListener;

    public OrderXmlHandler(final OrderListener orderListener) {
        this.orderListener = orderListener;
    }

    @Override
    public void startElement(final String uri, final String localName, final String qName,
            final Attributes attributes) throws SAXException {
        ElementParser parser;

        switch (localName) {
        case ADD_ORDER:
            parser = addOrderParser;
            break;
        case DELETE_ORDER:
            parser = deleteOrderParser;
            break;
        default:
            parser = defaultParser;
        }

        parser.processElement(localName, attributes);
    }

    public interface ElementParser {
       void processElement(final String localName, final Attributes attributes);
    }

    public class DefaultParser implements ElementParser {
        @Override
        public void processElement(final String localName, final Attributes attributes) {
            if (LOG.isLoggable(Level.INFO)) {
                LOG.info("Process " + localName + " with default processor");
            }
        }
    }

    public abstract class OrderParser<O extends Order> implements ElementParser {
        @Override
        public void processElement(final String localName, final Attributes attributes) {
            final O order = createOrder();

            order.setBook(attributes.getValue("book"));
            // что бы кешировать Iteger
            order.setOrderId(Integer.valueOf(Integer.parseInt(attributes.getValue("orderId"))));

            processCustom(order, attributes);

            pushOrder(order);
        }

        private void pushOrder(final O order) {
            orderListener.onOrder(order);
        }

        protected abstract O createOrder();

        protected void processCustom(final O order, final Attributes attributes) {}
    }

    public class AddOrderParser extends OrderParser<AddOrder> {
        @Override
        protected AddOrder createOrder() {
            return new AddOrder();
        }

        @Override
        protected void processCustom(final AddOrder order, final Attributes attributes) {
            order.setOperation(Operation.valueOf(attributes.getValue("operation")));
            order.setPrice(Double.parseDouble(attributes.getValue("price")));
            order.setVolume(Integer.parseInt(attributes.getValue("volume")));
        }
    }

    public class DeleteOrderParser extends OrderParser<DeleteOrder> {
        @Override
        protected DeleteOrder createOrder() {
            return new DeleteOrder();
        }
    }
}
