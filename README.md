Задание от Сбертеха:

Description

Program should create dynamic Order Book (https://en.wikipedia.org/wiki/Order_book_(trading)) according to orders from XML file (drive.google.com/file/d/0B4FxtBlfiwJhbERraGJ2RXVtR00/).

Two actions supported: new order, delete exiting order.


New order processing:

    Check book attribute and create order book if not yet exist

    Check whether new order matches or not with order book

    If new order full filled then it’s deleted and also matched order is updated (or deleted as well in case of full fill)

    If new order partially filled then both orders are updated.

    If not filled – just added to order book


Delete order processing:

Find order book (via book attribute), delete order by orderId attribute if found


Output format:

After processing all orders from XML file program should print to standard output following information in following format:


Order book: ${order_book_name}

BID ASK

Qty@Price – Qty@Price

10@100.1 – 11@100.2

4@100.0 – 14@100.21

98@99.98 – 14@100.23

----------- – 12@101.00


